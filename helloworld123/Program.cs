﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace helloworld123
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("---------------------DINO STUFF BEGINS ---------------------");

            //create a basic dino object.
            var tempDinosaur1 = new Dinosaur();
            string tempDinoName = "n2";
            double tempDinoHeight = 10;
            double tempDinoWeight = 12;
            string tempDinoTerrain = "AM";
            int tempDinoID = 2;

            var tempDinosaur2 = new Dinosaur(tempDinoName, tempDinoHeight, tempDinoWeight, tempDinoTerrain,tempDinoID);
          var tempDinosaur3 = new Dinosaur("n3",20,30,"CH",3);
            var tempDinosaur4 = new Dinosaur("n4", 70, 5, "IN", 4);
            var tempDinosaur5 = new Dinosaur("n5", 50, 5, "IN", 5);
            var tempDinosaur6 = new Dinosaur("n6", 40, 5, "CH", 6);
            var tempDinosaur7 = new Dinosaur("n7", 60, 5, "AM", 7);
            var tempDinosaur8 = new Dinosaur("n8", 90, 5, "AM", 8);
            var tempDinosaur9 = new Dinosaur("n9", 30, 5, "CH", 9);
            var tempDinosaur10 = new Dinosaur("n10", 80, 5, "CH", 10);
            var tempDinoString = tempDinosaur1.ToString();
          //  var tempDinosaur3 = new Dinosaur();
          // tempDinosaur3 = tempDinosaur3.input();
           // tempDinosaur3.display();
            Console.WriteLine(tempDinoString);
            var CollectionOfDino = new List<Dinosaur>();

            CollectionOfDino.Add(tempDinosaur1);
            CollectionOfDino.Add(tempDinosaur2);
            CollectionOfDino.Add(tempDinosaur3);
            CollectionOfDino.Add(tempDinosaur4);
            CollectionOfDino.Add(tempDinosaur5);
            CollectionOfDino.Add(tempDinosaur6);
            CollectionOfDino.Add(tempDinosaur7);
            CollectionOfDino.Add(tempDinosaur8);
            CollectionOfDino.Add(tempDinosaur9);
            CollectionOfDino.Add(tempDinosaur10);
            var collectionofdino_sorted = CollectionOfDino.OrderByDescending(x => x.Dinoheight).ToList();
            //DisplayDinoCollection(collectionofdino_sorted);
            

            showdinobasedoncountry(CollectionOfDino);
            //  CollectionOfDino.Add(tempDinosaur3);

            /* string name1 = "veer";
             function1();
             function2(name1);
             Console.WriteLine(function3(name1));*/
            FiveInputs(CollectionOfDino);
            Console.ReadKey();
        }

      
        private static void showdinobasedoncountry(List<Dinosaur> collectionOfDino)
        {

            var dinoINDIA = collectionOfDino.Select(x => x.Dinoterrain == "IN").ToList();
            var dinoAMERICA = collectionOfDino.Select(x => x.Dinoterrain == "AM").ToList();
            var dinoCHINA = collectionOfDino.Select(x => x.Dinoterrain == "CH").ToList();

            //DisplayDinoCollection(dinoINDIA);
            //          DisplayDinoCollection(dinoAMERICA);
            //        DisplayDinoCollection(dinoCHINA);
            
            var dinoINDIAlist = collectionOfDino.Select(x => x).Where(x => x.Dinoterrain=="IN").ToList();
            var dinoAMERICAlist = collectionOfDino.Select(x => x).Where(x => x.Dinoterrain == "AM").ToList();
            var dinoCHINAlist = collectionOfDino.Select(x => x).Where(x => x.Dinoterrain == "CH").ToList();
           /* Console.WriteLine("Enter country name to search specific dinos");
            string name = Console.ReadLine();
            if (name == "IN") { DisplayDinoCollection(dinoINDIAlist); }

            else if (name == "AM") { DisplayDinoCollection(dinoAMERICAlist); }
            else if (name == "CH") { DisplayDinoCollection(dinoCHINAlist);  }
            else
            {
                Console.WriteLine("Try Another");
            }*/
            
            var abc = 0;
        }
        private static void FiveInputs(List<Dinosaur> collectionofdino)
        {
            
           
                Console.WriteLine("Enter name of dino");
                string name = Console.ReadLine();

                Console.WriteLine("Enter height of dino");
                double height = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Enter weight of dino");
                double weight = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Enter terrain of dino");
                string terrain = Console.ReadLine();

                Console.WriteLine("Enter id of dino");
                int ID = Convert.ToInt32(Console.ReadLine());
                var dinoidnull = collectionofdino.Select(x => x).Where(x => x.DinoID == ID).FirstOrDefault();
                if(dinoidnull!=null)
                {
                    var LastDino = collectionofdino.Last();
                    var LastDinoID = LastDino.DinoID;
                    ID = LastDinoID + 1;
                }
                var tempDinoNew = new Dinosaur(name, height, weight, terrain, ID);
                collectionofdino.Add(tempDinoNew);
                Console.WriteLine("new dino with id" + ID + "added");
            DisplayDinoCollection(collectionofdino);
            Delete(collectionofdino);
            
        }
        private static void Delete(List<Dinosaur> collectionOfDino)
        {
            Console.WriteLine("ENter iD of Dino to dElete");
            int id = Convert.ToInt32(Console.ReadLine());
            var newid=collectionOfDino.Select(x=>x).Where(x=>x.DinoID==id).FirstOrDefault();
            if(newid==null)
            {
                Console.WriteLine("No such dino exists with Id no" + id);
            }
            collectionOfDino.Remove(newid);
            Console.WriteLine("Id removed");
            DisplayDinoCollection(collectionOfDino);

        }

        private static void DisplayDinoCollection(List<Dinosaur> collectionOfDino)
        {
            //throw new NotImplementedException();

            //get total dinos.
            var totalDinos = collectionOfDino.Count;
           
            var message = "";

            //display total dinos for reference
            message = "Total Number of Dinos - " + totalDinos;
            Console.WriteLine(message);
            

            //loop through each dino.
            foreach (var dino in collectionOfDino)
            {
                //display dino details using the already existing display function
                dino.display();
              
                //put a simple line to indicate that a new dino will be displated in the next iteration
                message = "--------------------";
                Console.WriteLine(message);
            }
        }
        static string function3(string name)
        {
            return "Hello" + name;
        }

        static void function1()
        {
            Console.WriteLine("I am in function1 ");
        }
        static void function2(string name)
        {
            Console.WriteLine("hello  "  + name +  "I am in function 2");
        }
        static void basictypestuf()
        {
            
            bool flag;
            var m = Console.ReadLine().ToString();
            int n=0;
            try
            {
                n = Convert.ToInt32(m);

            }
            catch(Exception e)
            {
                Console.WriteLine("error- {0}", e.ToString());
                n = 10;
            }
            if(n>5)
            {
                flag = true;
            }
            else
            {
                flag = false;
            }
            Console.WriteLine(flag);
        }
    }
}
