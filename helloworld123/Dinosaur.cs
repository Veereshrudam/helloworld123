﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace helloworld123
{
    //this class describes characteristcic for class dinosaur
    class Dinosaur
    {
        // four things which we discuss in this class to describe dinosaur
        //name
        public string Dinoname { get; set; }
        //height of dinosaur
        public double Dinoheight { get; set; }
        //weight of dinosaur
        public double Dinoweight { get; set; }
        //terrain (land,water,forest)
        public string Dinoterrain { get; set; }

        public int DinoID { get; set; }

        //TODO

        //default constructor
       public Dinosaur()
        {
            Dinoname = "n1";
            Dinoheight = 100;
            Dinoweight = 15;
            Dinoterrain = "IN";
            DinoID = 1;
            
        }

        //custom constructor with parameters
        public Dinosaur(string Dinoname, double Dinoheight, double Dinoweight, string Dinoterrain,int DinoID)
        {
            this.Dinoheight = Dinoheight;
            this.Dinoname = Dinoname;
            this.Dinoterrain = Dinoterrain;
            this.Dinoweight = Dinoweight;
            this.DinoID = DinoID;
            
        }
        //function do take input for Dino
        public Dinosaur input()
        {
            Console.WriteLine("Enter name of dino\n");
            string name1 = Console.ReadLine();
            Console.WriteLine("Enter height of dino\n");
            double height1 =Convert.ToInt32( Console.ReadLine());
            Console.WriteLine("Enter weight of dino\n");
            double weight1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter terrain of dino\n");
            string terrain1 = Console.ReadLine();
            Console.Write("Enter id of dino\n");
            int ID1 = Convert.ToInt32(Console.ReadLine());
            var obj4 = new Dinosaur(name1, height1, weight1, terrain1,ID1);
            return obj4;
        }
     
        //function to display Dino
        public void display()
        {
            Console.WriteLine("Name of dino :"+Dinoname);
            Console.WriteLine("Height of dino :"+Dinoheight);
            Console.WriteLine("Weight of dino :"+Dinoweight);
            Console.WriteLine("Terrain of dino :"+Dinoterrain);
            Console.WriteLine("ID of dino: " + DinoID);
            Console.WriteLine("-------------//-----------------");
        }


        //function to print all Dino 
      
    }
}
